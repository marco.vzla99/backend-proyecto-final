/*
path: api.logins
*/

const { Router, response } = require('express');
const { check } = require('express-validator');

const { crearUsuario, login, renewToken } = require('../controllers/auth');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const router = Router();

router.post('/new', [
    check ('nombre', 'Nombre obligatorio').not().isEmpty(),
    check ('password', 'Contraseña obligatorio').not().isEmpty(),
    check ('email', 'Correo obligatorio').isEmail(),
    validarCampos
],crearUsuario); 

router.post('/', [
    check ('password', 'Contraseña obligatorio').not().isEmpty(),
    check ('email', 'Correo obligatorio').isEmail(),
], login);

router.get('/renew', validarJWT, renewToken );

module.exports = router;